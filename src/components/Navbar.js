import React from "react";
import { Link } from "react-router-dom";
import logo from "../assets/svg/Fondi_F.svg";

//Import Material UI
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
//import { makeStyles } from "@material-ui/core/styles";

/* const useStyles = makeStyles({
	loginStyle: {
		margin: "5px",
	},
}); */

const Navbar = () => {
	//const classes = useStyles();

	return (
		<AppBar elevation={0}>
			<Toolbar className="nav-container">
				<div className="nav-layout">
					<Button
						color="inherit"
						component={Link}
						to="/"
						style={{ backgroundColor: "transparent" }}>
						<img src={logo} alt="Home" className="logo" />
					</Button>
					<Button
						component={Link}
						to="/"
						style={{ backgroundColor: "transparent" }}>
						<Typography variant="h5">Fondi</Typography>
					</Button>
				</div>
				{/* <div>
					<Button
						className={classes.loginStyle}
						variant="outlined"
						color="primary"
						component={Link}
						to="/login">
						Logi sisse
					</Button>
					<Button
						className={classes.loginStyle}
						variant="contained"
						color="primary"
						component={Link}
						to="/signup">
						Loo konto
					</Button>
				</div> */}
			</Toolbar>
		</AppBar>
	);
};

export default Navbar;
