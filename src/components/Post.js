import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
//MUI
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";

const styles = {
	card: {
		position: "relative",
		display: "flex",
		marginBottom: 20,
	},
	image: {
		minWidth: 200,
	},
	content: {
		padding: 25,
		objectFit: "cover",
	},
};

const Post = (props) => {
	dayjs.extend(relativeTime);
	const {
		classes,
		post: { body, createdAt, userImage, username, postId, commentCount },
	} = props;
	return (
		<Card className={classes.card}>
			<CardMedia
				image={
					"https://firebasestorage.googleapis.com/v0/b/fondi-462de.appspot.com/o/814445632750.jpg?alt=media&token=18e3935c-874c-4484-847f-1dc2f7d0bbcf"
				}
				title="Profile image"
				className={classes.image}
			/>
			<CardContent className={classes.content}>
				<Typography
					variant="h5"
					component={Link}
					to={`/users/${username}`}
					color="primary">
					{username}
				</Typography>
				<Typography variant="body2" color="textSecondary">
					{dayjs(createdAt).fromNow()}
				</Typography>
				<Typography variant="body1">{body}</Typography>
			</CardContent>
		</Card>
	);
};

export default withStyles(styles)(Post);
