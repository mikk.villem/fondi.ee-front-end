import React, { useState } from "react";
import { Link } from "react-router-dom";
import { isEmail } from "../util/validations";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { makeStyles } from "@material-ui/core/styles";
import axios from "axios";

const useStyles = makeStyles({
	cta: {
		marginTop: "60px",
	},
});

export default function FormDialog() {
	const classes = useStyles();

	const [open, setOpen] = useState(false);
	const [email, setEmail] = useState("");
	const [loading, setLoading] = useState(false);
	const [errors, setError] = useState({});
	const [step, setStep] = useState(1);

	const handleClickOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
		setEmail("");
		setError({});
		//setStep(1);
	};

	const submitSuccess = () => {
		setStep(2);
		setTimeout(handleClose, 1500);
		setLoading(false);
	};

	const handleSubmit = (event) => {
		event.preventDefault();
		setLoading(true);

		const newSubscription = {
			email: email,
		};

		if (!isEmail(newSubscription.email)) {
			setLoading(false);
			setError({ email: "Palun sisesta email õigel kujul" });
			return;
		}

		axios
			.post("/newsletter/subscribe", newSubscription)
			.then((response) => {
				submitSuccess();
			})
			.catch((error) => {
				if (error.response) {
					// The request was made and the server responded with a status code
					// that falls out of the range of 2xx
					console.log(error.response.data.email);
					console.log(error.response.status);
				} else if (error.request) {
					// The request was made but no response was received
					// `error.request` is an instance of XMLHttpRequest in the browser and an instance of
					// http.ClientRequest in node.js
					console.log(error.request);
				} else {
					// Something happened in setting up the request that triggered an Error
					console.log("Error", error.message);
				}
				setError(error.response.data);
				setLoading(false);
			});
	};

	const handleChange = (event) => {
		setEmail(event.target.value);
		setError("");
	};

	return (
		<div>
			<Button
				className={classes.cta}
				variant="contained"
				color="secondary"
				to="/signup"
				onClick={handleClickOpen}>
				Liitu uudiskirjaga
			</Button>
			<Dialog
				open={open}
				onClose={handleClose}
				aria-labelledby="form-dialog-title">
				<DialogTitle id="form-dialog-title">
					{step === 1 ? "Telli uudiskiri" : "Täname!"}
				</DialogTitle>
				{step === 1 ? (
					<div>
						<DialogContent>
							<DialogContentText>
								Et meie tegemistega kursis olla ning saada uue
								ühisrahastusportaali esimeste kasutajate eeliseid, kirjuta
								lahtrisse oma e-mail. Vajutades "Kinnita" nõustud meie{" "}
								<Link to="/privaatsus" target="_blank">
									privaatsuspoliitikaga.
								</Link>
							</DialogContentText>
							<TextField
								onChange={handleChange}
								autoFocus={false}
								margin="dense"
								id="email"
								name="email"
								label="Email"
								type="email"
								helperText={errors.email}
								error={errors.email ? true : false}
								value={email}
								fullWidth
							/>
						</DialogContent>
						<DialogActions>
							<Button onClick={handleClose} color="primary" type="reset">
								Katkesta
							</Button>
							<Button
								onClick={handleSubmit}
								color="primary"
								type="submit"
								form="newsLetter"
								disabled={loading}>
								Kinnita
							</Button>
						</DialogActions>
					</div>
				) : (
					<DialogContent>
						<DialogContentText>
							Olete lisatud uudiskirja tellijate hulka.
						</DialogContentText>
					</DialogContent>
				)}
			</Dialog>
		</div>
	);
}
