import React, { useState, useEffect } from "react";
import axios from "axios";
import Post from "../components/Post";

function Discover(props) {
	const [posts, setPosts] = useState(null);

	useEffect(() => {
		axios
			.get("/posts")
			.then((res) => {
				console.log(res.data);
				setPosts(res.data);
			})
			.catch((err) => {
				console.log(err);
			});
	}, []);

	return (
		<div>
			{posts ? (
				posts.map((post) => <Post key={post.postId} post={post} />)
			) : (
				<p>No posts yet</p>
			)}
		</div>
	);
}

export default Discover;
