import React, { useEffect } from "react";
// MUI
import { Typography } from "@material-ui/core";
//Imgs
import undrawSavings from "../assets/svg/undraw_Savings.svg";
import undrawBlob from "../assets/svg/undraw_blob.svg";
//Comps
import Modal from "../components/Modal";

const Home = (props) => {
	useEffect(() => {
		// Update the document title using the browser API
		console.log("Home page");
	});

	return (
		<div>
			<img src={undrawBlob} alt="blob shape" className="blob" />
			<img src={undrawSavings} alt="savings pig" className="savings" />
			<div className="hero-container">
				<Typography variant="h3" className="hero-title">
					Astu samm loomingulise vabaduse poole
				</Typography>
				<Typography variant="subtitle1" className="hero-subtitle">
					Fondi on varsti avatav püsimaksetel põhinev ühistoetusplatvorm, mille
					läbi saad anda oma fännidele võimaluse su tegevusi püsivalt ja
					väärikalt toetada.
				</Typography>
				<Modal />
			</div>
		</div>
	);
};

export default Home;
