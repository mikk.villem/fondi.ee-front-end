import React, { useState, useEffect } from "react";

import { Paper, Typography } from "@material-ui/core";
import ReactMarkdown from "react-markdown";
import policyText from "./content/privacyPolicy.md";

const PrivacyPolicy = () => {
	const [policy, setPolicy] = useState("");

	useEffect(() => {
		fetch(policyText)
			.then((response) => response.text())
			.then((text) => {
				setPolicy(text);
			});
	});

	return (
		<Paper elevation={3} className="paper">
			<Typography variant="body1">
				<ReactMarkdown source={policy} />
			</Typography>
		</Paper>
	);
};

export default PrivacyPolicy;
