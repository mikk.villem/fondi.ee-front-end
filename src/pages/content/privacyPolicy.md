### PRIVAATSUSPOLIITIKA

#### Kehtiv alates 01.06.2020

Käesolev dokument kirjeldab, kuidas Muusikute Koda MTÜ (registrikood:80348355, aadress: Vabarna tn 4 Viljandi Viljandimaa 71017, e-post: info@fondi.ee, edaspidi “ettevõte”) töötleb vastutava töötlejana loodava ühisrahastusportaali Fondi.ee tegevuse kogutud kasutajate ja uudiskirjaga liitujate andmeid.

Isikuandmete töötlemisel lähtume Eesti Vabariigi ning Euroopa Liidu asjakohastest õigusaktidest.

#### Isikuandmete töötlemise eesmärk

Ettevõte töötleb meie külastajate ja klientide kohta käivaid andmeid järgmistel eesmärkidel:

Uudiskiri – Isikuandmeid kogume Fondi.ee uudiskirja saatmiseks isikutele, kes on andnud selleks nõusoleku. Uudiskirja saamiseks esitab isik oma e-posti. Uudiskirja tellimiseks antud nõusoleku saab isik tagasi võtta igal ajal uudiskirjas toodud lingi kaudu. Uudiskirjaga seotud isikuandmeid töötleme nõusoleku kehtivuse jooksul.

#### Isikuandmete töötlejad

Kui isik on andnud nõusoleku uudiskirja saamiseks, edastab Fondi.ee isiku andmed lepingupartnerile, kes valmistab ette, saadab või teeb tehniliselt võimalikuks uudiskirja saatmise. Fondi.ee poolt töödeldavatele isikuandmetele on juurdepääs meie IT-teenuse osutajal.

#### Andmesubjekti õigused

Isikul on õigus teha päringuid teda puudutavatele isikuandmete töötlemise kohta ning nõuda nende parandamist või kustutamist või isikuandmete töötlemise piiramist. Samuti on isikul õigus esitada vastuväide isikuandmete töötlemisele, ning taotleda isikuandmete ülekandmist, kui see on tehniliselt võimalik. Nõusoleku alusel töödeldavate isikuandmete kohta käiva nõusoleku võib isik igal ajal tagasi võtta, ilma et see mõjutaks enne tagasivõtmist nõusoleku alusel toimunud töötlemise seaduslikkust. Isikuandmete töötlemisega seoses on isikul õigus esitada kaebus Andmekaitse Inspektsioonile (www.aki.ee).
Isikuandmete töötlemisega seotud õiguste teostamiseks või täiendava informatsiooni saamiseks palume pöörduda info@fondi.ee.

#### Isikuandmete turvalisuse tagamine

Isikuandmeid hoitakse ja töödeldakse koostöös lepingupartneritega digitaalselt ja turvalistes keskkondades. Ligipääs isikuandmetele on tagatud üksnes ettevõtte juhtidele, töötajatele ja lepingupartneritele, kellel on isikuandmeid vajalik käidelda ettevõttega sõlmitud lepingulisest kohustusest tulenevalt. Lepingupartneritel on kohustus järgida isikuandmete kaitset reguleerivaid õigusakte.
