import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

//MUI
import { MuiThemeProvider } from "@material-ui/core/styles";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import Container from "@material-ui/core/Container";

//Components
import Navbar from "./components/Navbar";

//Redux
import { Provider } from "react-redux";
import store from "./redux/store";

//Pages
import home from "./pages/home";
import login from "./pages/login";
import signup from "./pages/signup";
import privacyPolicy from "./pages/privacyPolicy";
//Styles
import "./App.css";
import themeBase from "./util/theme";

import axios from "axios";

const theme = createMuiTheme(themeBase);

axios.defaults.baseURL =
	"https://europe-west1-fondi-462de.cloudfunctions.net/api";

theme.typography.h3 = {
	fontFamily: [
		"Montserrat",
		"Roboto",
		'"Helvetica Neue"',
		"Arial",
		"sans-serif",
	].join(","),
	fontSize: "1.5rem",
	fontWeight: 500,
	"@media (min-width:463px)": {
		fontWeight: 600,
	},
	"@media (min-width:855px)": {
		fontSize: "2rem",
	},
	[theme.breakpoints.up("md")]: {
		fontSize: "3rem",
	},
};

theme.typography.subtitle1 = {
	fontSize: "1rem",
	fontWeight: 400,
	"@media (min-width:855px)": {
		fontSize: "1rem",
	},
	[theme.breakpoints.up("md")]: {
		fontSize: "1.1125rem",
	},
};

function App() {
	return (
		<MuiThemeProvider theme={theme}>
			<Provider store={store}>
				<div className="App">
					<Router>
						<Navbar />
						<Container className="container">
							<Switch>
								<Route exact path="/" component={home} />
								<Route exact path="/login" component={login} />
								<Route exact path="/registreeru" component={signup} />
								<Route exact path="/privaatsus" component={privacyPolicy} />
							</Switch>
						</Container>
						<div className="circle"></div>
						<div className="triangle"></div>
						<div className="square"></div>
					</Router>
				</div>
			</Provider>
		</MuiThemeProvider>
	);
}

export default App;
